
citbx_use "dockerimg"

target_build_exec_hook() {
    local hook_function=$1
    if [ -f "targets/$TARGET_IMAGE_NAME/build-hooks.sh" ]; then
        cd targets/$TARGET_IMAGE_NAME
        source "build-hooks.sh"
        if [[ "$(type -t $hook_function)" == "function" ]]; then
            $hook_function
        else
            print_note "HOOK[$hook_function]: Function $hook_function not found, skipping this hook"
        fi
    else
        print_note "HOOK[$hook_function]: File targets/$TARGET_IMAGE_NAME/build-hooks.sh not found, skipping this hook"
        return
    fi
    cd "$CI_PROJECT_DIR"
}

job_setup() {
    # Only on workstation, set the suitable tag prefix if not set
    CI_COMMIT_TAG="$CI_JOB_NAME/${CI_COMMIT_TAG#$CI_JOB_NAME/}"
}

job_main() {
    TARGET_IMAGE_NAME=$CI_JOB_NAME
    pattern='^'"$TARGET_IMAGE_NAME"'/.*$'
    if ! [[ $CI_COMMIT_TAG =~ $pattern ]]; then
        print_critical "This job cannot be launched with the following tag '$CI_COMMIT_TAG'" \
                        "This error is probably due to:" \
                        " * on a Gitlab runner: Missing or incorrect 'only' tag in the .gitlab-ci.yml - You can put this one:" \
                        "    only:" \
                        "        - /^$TARGET_IMAGE_NAME\/.*\$/" \
                        " * on your local worspace: You have launched ci-toolbox $CI_JOB_NAME with the wrong tag - Try with the additional option:" \
                        "    --image-tag $TARGET_IMAGE_NAME/x.y.z"
    fi
    TARGET_IMAGE_VERSION=${CI_COMMIT_TAG#$TARGET_IMAGE_NAME/}
    TARGET_CI_REGISTRY_IMAGE_TAG="$CI_REGISTRY_IMAGE/$TARGET_IMAGE_NAME:$TARGET_IMAGE_VERSION"
    target_build_exec_hook target_docker_build_before
    # Build docker image
    TARGET_DOCKER_BUILD_ARGS+=(-t "$TARGET_CI_REGISTRY_IMAGE_TAG")
#     TARGET_DOCKER_BUILD_ARGS+=(--build-arg "CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE")
    docker build "${TARGET_DOCKER_BUILD_ARGS[@]}" "targets/$TARGET_IMAGE_NAME/"
    if [ -n "$CI_BUILD_TOKEN" ]; then
        docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
        docker push "$TARGET_CI_REGISTRY_IMAGE_TAG"
    fi
}

job_after() {
    local retcode=$1
    target_build_exec_hook target_docker_build_after
    if [ $retcode -eq 0 ]; then
        print_info "Image \"$TARGET_CI_REGISTRY_IMAGE_TAG\" successfully generated"
    fi
}
