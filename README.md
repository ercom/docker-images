# Docker image builder

This project is designed to symplify the docker image management

* [Target GitLab-CI job definition](#target-gitlab-ci-job-definition)
* [Target docker image definition](#target-docker-image-definition)
* [Optionally add custom docker build hooks](#optionally-add-custom-docker-build-hooks)
* [Annexes](#annexes)
  + [The ci-toolbox command](#the-ci-toolbox-command)

Steps to create a new target:
1. Add a target definition into the `.gitlab-ci.yml` file
2. Add the `Dockerfile` and extra ressources into the `targets/my-new-target/` directory
3. Optionally, add `targets/my-new-target/build-hooks.sh` to perfom additional actions before and after the `docker build`

Steps to build a target:
* **On GitLab**: Create a tag using the format `target-name/version`
* **Locally** using the [ci-toolbox](#the-ci-toolbox-command) command: `ci-toolbox target-name --image-tag version`

## Target GitLab-CI job definition

The job into the `.gitlab-ci.yml` must follow this model:
```yaml
my-new-target:
  only:
    - /^my-new-target\/.*$/
  <<: *build-template
```

## Target docker image definition

Put or define the Dockerfile and associated ressources into the directory `targets/my-new-target/`

## Optionally add custom docker build hooks

You can perfom additional actions before and after the `docker build` by adding a file `targets/my-new-target/build-hooks.sh` following this model:
```bash
target_docker_build_before() {
    echo "Executed before docker build, useful to perform some action that cannot be performed into the Dockerfile"
}

target_docker_build_after() {
    echo "Executed before docker build, useful to perform some action like temporary files cleanup"
}

```

## Annexes

### The ci-toolbox command
The `ci-toolbox` command is a tool, provided by the `tools/gitlab-ci/run.sh setup` execution, to run Gitlab-CI jobs on your local workstation (see the [project page](https://gitlab.com/ercom/citbx4gitlab) for more information)

